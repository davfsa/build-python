#!/bin/bash

################################################################################
# Version info logic                                                           #
################################################################################

MAJOR=3
MINOR=8
MICRO=0

VERSION=${MAJOR}.${MINOR}
FULL_VERSION=${VERSION}.${MICRO}

################################################################################
# Prerequisites                                                                #
################################################################################


function install-deps() {
    sudo apt update
    # sudo apt -y full-upgrade
    
    set +e
    for dep in $(xargs <<< "build-essential git libbz2-dev libc6-dev libdb5.3-dev \
                            libexpat1-dev libffi6 libffi-dev libgdbm-dev \
                            liblzma-dev libncurses5-dev libncursesw5-dev \
                            libreadline6-dev libreadline-dev libsqlite3-dev \
                            libssl-dev linux-headers-$(uname -r) make openssl \
                            python-dev python-pip python-setuptools python-smbus \
                            tcl-dev tk tk-dev zlib1g-dev"); do
        sudo apt install $dep -y || echo -e "\e[1;31mFAILED TO INSTALL $dep\e[0m"
    done
    set -e
}


# Speed the make process up significantly.
LOGICAL_CORES=$(grep -c ^processor /proc/cpuinfo || echo "4")


################################################################################
# Utility bits and bobs                                                        #
################################################################################

function exists() {
    which "${*}" > /dev/null 2>&1
    return ${?}
}



# Target URL
URL="https://www.python.org/ftp/python/${FULL_VERSION}/Python-${FULL_VERSION}.tgz"

################################################################################
# Installation logic                                                           #
################################################################################

function build() {
    # Keeps sudo alive in the background for the duration of this process to prevent reprompting.
    while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &
    set -x
    set -e
    which dpkg apt && install-deps
    # Prepare place to build.
    cd /tmp
    sudo wget -qO- ${URL} | tar xzv
    DIRNAME=Python-${FULL_VERSION}
    cd ${DIRNAME}
    sudo ./configure
    # Build source code
    sudo make -j${LOGICAL_CORES}
    # Run unit tests
    # Install
    sudo make altinstall -j${LOGICAL_CORES}
    cd /tmp
    sudo rm ${DIRNAME} -Rvf
    sudo python${VERSION} -m ensurepip
    sudo python${VERSION} -m pip install --upgrade pip
    echo "Finished!"
    echo -n "Installed "
    python${VERSION} -V
    set +e
    set +x
}

time build
